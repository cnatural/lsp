"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_languageserver_1 = require("vscode-languageserver");
const vscode = require("vscode");
const log4js_1 = require("log4js");
const utils_1 = require("./utils");
// configure logging
log4js_1.configure({
    appenders: {
        lsp_demo: {
            type: "dateFile",
            filename: "lsp-logs/lsp_demo",
            pattern: "yyyy-MM-dd-hh.log",
            alwaysIncludePattern: true,
        },
    },
    categories: { default: { appenders: ["lsp_demo"], level: "debug" } }
});
const logger = log4js_1.getLogger("lsp_demo");
//create connection for lsp
let connection = vscode_languageserver_1.createConnection(vscode_languageserver_1.ProposedFeatures.all);
let documents = new vscode_languageserver_1.TextDocuments();
// initialises connection
connection.onInitialize((params) => {
    let capabilities = params.capabilities;
    return {
        capabilities: {
            hoverProvider: true,
            textDocumentSync: documents.syncKind,
            completionProvider: { resolveProvider: false, triggerCharacters: ['.'] }
        }
    };
});
// to be removed. Debugging purposes. Tells me that lsp is connected and initialised
connection.onInitialized(() => {
    logger.info("Testing");
});
connection.onHover(docPos => utils_1.action.onHover(docPos, logger));
let functionNames = [];
let classList = [];
var objectDict = [];
documents.onDidChangeContent(change => {
    let diag = utils_1.action.contentChange(change.document, logger);
    connection.sendDiagnostics(diag);
    //get class names
    classList = utils_1.action.getClassList(change.document, logger);
    //get object names
    objectDict = utils_1.action.logObject(change.document, logger, classList);
    //get function names
    functionNames = utils_1.action.logNewFunction(change.document, logger);
});
connection.onCompletion(docPos => {
    var return_func = [];
    let completionWord = utils_1.action.getCompletionWord(docPos, logger, documents);
    for (let variable in objectDict) {
        if (completionWord == objectDict[variable].key) {
            for (let item in functionNames) {
                if (functionNames[item].key == objectDict[variable].value) {
                    for (let val in functionNames[item].value) {
                        return_func.push({ label: functionNames[item].value[val] });
                    }
                }
            }
        }
    }
    logger.info(return_func);
    return return_func;
});
const tokenTypes = [];
const tokenModifiers = [];
const legend = new vscode.SemanticTokensLegend(tokenTypes, tokenModifiers);
documents.listen(connection);
connection.listen();
//# sourceMappingURL=server.js.map