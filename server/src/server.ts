import {
  createConnection,
  DiagnosticSeverity,
  TextDocuments,
  ProposedFeatures,
  InitializeParams,
  TextDocumentSyncKind,
  ConnectionOptions,
  Hover,
  TextDocumentPositionParams,
  Diagnostic,
  CompletionItemKind
} from 'vscode-languageserver';

import { configure, getLogger } from "log4js";

import { action } from './utils';

// configure logging
configure({
  appenders: {
    lsp_demo: {
      type: "dateFile",
      filename: "lsp-logs/lsp_demo",
      pattern: "yyyy-MM-dd-hh.log",
      alwaysIncludePattern: true,
    },
  },
  categories: { default: { appenders: ["lsp_demo"], level: "debug" } }
});

const logger = getLogger("lsp_demo");

//create connection for lsp
let connection = createConnection(ProposedFeatures.all);
let documents = new TextDocuments();

// initialises connection
connection.onInitialize((params: InitializeParams) => {
  let capabilities = params.capabilities;
  return {
    capabilities: {
      hoverProvider: true,
      textDocumentSync: documents.syncKind,
      completionProvider: {resolveProvider: false, triggerCharacters: ['.']}
    }
  };
});

// to be removed. Debugging purposes. Tells me that lsp is connected and initialised
connection.onInitialized(() => {
  logger.info("Testing");
});


connection.onHover(docPos => 
  {
    let hover="";
    let text = action.hoverWord(docPos, logger, documents);
    logger.info(text);
    varCommentsDict.forEach(element => {
      text.forEach(word => {
        if(element.key==word)
        {
          hover=element.value
        }
      });

    });
    return {
      contents:
      {
        language: 'cn',
        value: hover
      }
    };

  });

let functionNames = [];
let classList = [];
let objectDict = [];
let varCommentsDict=[];
documents.onDidChangeContent(change => { 
  let diag = action.contentChange(change.document, logger)
  connection.sendDiagnostics(diag);
  //get class names
  classList = action.getClassList(change.document, logger)
  //get object names
  objectDict = action.logObject(change.document, logger, classList)
  //get function names
  functionNames = action.logNewFunction(change.document, logger)

  varCommentsDict = action.mapCommentToVar(change.document, logger)
});


connection.onCompletion(docPos => {
  var return_func=[]
  let completionWord = action.getCompletionWord(docPos, logger, documents)

  for (let variable in objectDict)
  { 
    if (completionWord == objectDict[variable].key)
    { 
      for (let item in functionNames)
      {
        if(functionNames[item].key == objectDict[variable].value)
        {
          for (let val in functionNames[item].value)
          {
            return_func.push({label: functionNames[item].value[val]})
          }
        }
      }
    }

  }
  logger.info(return_func)
  return return_func
})



documents.listen(connection);
connection.listen();
