import {
  Diagnostic,
  DiagnosticSeverity,
  Hover,
  TextDocuments,
  integer,
  PublishDiagnosticsNotification,
  PublishDiagnosticsParams,
  TextDocumentPositionParams,
  createProtocolConnection
} from 'vscode-languageserver';

import { Logger } from "log4js";

export class action {

  public static mapCommentToVar(doc, log: Logger)
  {
    var varDefinitions = ["int", "string", "bool", "void"];
    let text=doc.getText();
    let lineStart = true;
    let lineNum = 0;
    let line = "";
    var varCommentsDict = [];
    
    for (let i in text) {
      if (text[i] == "\n") {
        lineStart = true
        lineNum++
        if (line.match("\/\/.*"))
        {
          varDefinitions.forEach(element => {
            if(line.match(element))
            {
              let varName = line.split("//");
              let comment = varName[1];

              if (varName[0].match("="))
              {
                varName=varName[0].split("=");
              }
              varName = varName[0].split(" ");
              let size= varName.length;
              varCommentsDict.push({key:varName[size-2], value:comment});
              
            }
          });
        }
        line="";
      }
      else{
        line=line.concat(text[i]);
      }
      
  }
  log.info(varCommentsDict);

  return varCommentsDict;
  }

  public static hoverWord(textDocumentPosition: TextDocumentPositionParams, log: Logger, doc: TextDocuments) 
  {
    let content = doc.all()[0].getText()

    let count = 0
    let line = ""
    for (let i = 0; i < content.length; i++) {
      if (count == textDocumentPosition.position.line) {
        line = line.concat(content[i])
      }
      if (content[i] == "\n") {
        if(count == textDocumentPosition.position.line)
        {
          let val = line.split("=");
          return val[0].split(" ");
        }
        
        count++

      }
    }
  }

  public static checkTabs(text, logger: Logger) {
    let count = 0;
    let lineStart = false;
    let incorrectSpaces = [];
    for (let i in text.getText()) {
      if (text.getText()[i] == "\n") {
        lineStart = true
        if (count != 0) {
          count = 0
        }
      }
      else if (text.getText()[i] == " ") {
        if (lineStart) {
          count++
        }
      }
      else {
        if (count != 0 && count % 4 != 0) {
          incorrectSpaces.push({
            start: parseInt(i),
            end: parseInt(i) - count
          })
        }
        lineStart = false
        count = 0
      }
    }
    return incorrectSpaces
  }

  private static diagnose = (doc) => ({ start, end }) => ({
    severity: DiagnosticSeverity.Warning,
    range: {
      start: doc.positionAt(start),
      end: doc.positionAt(end)
    },
    message: "Inconsistant Spaces"
  })

  public static contentChange(change, logger: Logger) {

    let highlight = this.checkTabs(change, logger)

    let diag = highlight.map(this.diagnose(change))

    return {
      uri: change.uri,
      diagnostics: diag
    };
  }

  public static logNewFunction(change, logger: Logger) {
    var classRegex = new RegExp('^\s*class .*')
    var funcRegex = new RegExp('^    [A-z].*\\(')
    var line = ""
    var funcNames = []
    var classDef = false
    var classList = []
    var classFunc = []
    for (let i in change.getText()) {
      if (change.getText()[i] == "\n") {
        if (line.match(classRegex)) {
          let className = line.split(" ")
          classList.push(className[1])
          classFunc.push({ key: className[1], value: "" })
          if (classDef == true) {
            classFunc[classFunc.length - 2].value = funcNames
            funcNames = []
          }
          classDef = true
        }
        else if (classDef && line.match(funcRegex)) {
          var splitLine = line.split("(")
          splitLine = splitLine[0].split(" ")
          if (splitLine[splitLine.length - 1].includes(".") == false && classFunc[classFunc.length - 1].key != splitLine[splitLine.length - 1]) {
            funcNames.push(splitLine[splitLine.length - 1])
          }
        }
        line = ""
      }
      else {
        line = line.concat(change.getText()[i])
      }

    }
    classFunc[classFunc.length - 1].value = funcNames
    return classFunc
  }

  public static logObject(change, logger: Logger, classList) {
    var line = ""
    var classObjectDict = []
    for (let i in change.getText()) {
      if (change.getText()[i] == "\n") {
        var objectCheck = line.split(" ")
        for (let j = 0; j < objectCheck.length; j++) {
          if (objectCheck[j] == 'class') {
            break
          }
          else {
            for (let i = 0; i < classList.length; i++) {
              if (objectCheck[j] == classList[i]) {
                classObjectDict.push({ key: objectCheck[j + 1], value: objectCheck[j] })
              }
            }
          }
        }

        line = ""
      }
      else {
        line = line.concat(change.getText()[i])
      }
    }
    return classObjectDict
  }

  public static getClassList(change, logger: Logger) {
    var classRegex = new RegExp('class .*')
    var line = ""
    var classList = []
    for (let i in change.getText()) {
      if (change.getText()[i] == "\n") {
        if (line.match(classRegex)) {
          let className = line.split(" ")
          classList.push(className[1])
        }
        line = ""
      }
      line = line.concat(change.getText()[i])
    }
    return classList
  }

  public static getCompletionWord(textDocumentPosition: TextDocumentPositionParams, logger: Logger, doc: TextDocuments) {
    let content = doc.all()[0].getText()
    let count = 0
    let line = ""
    for (let i = 0; i < content.length; i++) {
      if (count == textDocumentPosition.position.line) {
        //if content is the trigger character, return object
        if (content[i] == ".") {
          let returnLine = line.split(" ")
          return returnLine[returnLine.length - 1]
        }
        line = line.concat(content[i])
      }
      if (content[i] == "\n") {
        count++
      }
    }
  }
}
