# CNatural Language Server Protocol README
Language Server Protocol for C Natural Language:
https://gitlab.com/cnatural/cn

## Current Features

All features listed work on vscode.
* Syntax highlighter - most syntax highlighted. Complete list coming soon.
* Hover functionality is partially implemented.
    * Can hover, but it only shows the word test at present
* Logging system exists and used for all current features
* Tab warning notification
* Local Autocomplete
* Vim Compatibility


## Near Future Features

* Vim and other IDE and Editor compatability.
* Symantic Highlighter

## Use with coc-vim
* [Install coc vim](https://github.com/neoclide/coc.nvim) 
* Build server folder with ```npm i```
* Add cn filetype to coc.vim.
* ```autocmd BufNewFile,BufRead *.cn setlocal filetype=cnatural```
* Update coc-settings.json.